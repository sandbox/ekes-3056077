(function ($, Drupal, drupalSettings, pca) {
  /**
   * Address field autocomplete, powered by Loqate.
   */
  Drupal.behaviors.addressAutocomplete = {
    attach: function (context, settings) {
      var loqateOptions = drupalSettings.addressLoqate; 
      // Check the loqate options have been defined before continuing.
      if (typeof loqateOptions !== 'object') {
        return;
      }

      var addresses = $('.address-loqate', context);
      addresses.each(function () {
        var dataSelector = this.getAttribute('data-drupal-selector');

        // Selector, which can be changed, and has address field complexity.
        var country = $('select.country', this).val();
        // Straightforward hidden single country.
        if (typeof country == 'undefined') {
          var field = $('input[data-drupal-selector=' + dataSelector + '-address-country-code]', this);
          var country = field.val();
        }

        if (country.length) {
          // This code is triggered every time the country is changed and the field recreated by ajax.
          // So we can force the country search to the selected country.
          loqateOptions.search = { countries: $('select.country', this).val() };

          var loqateFields = [
            { element: 'search', field: '' },
            { element: "-address-organization", field: "Company", mode: pca.fieldMode.DEFAULT | pca.fieldMode.PRESERVE },
            { element: '-address-address-line1', field: 'Line1' },
            { element: '-address-address-line2', field: 'Line2', mode: pca.fieldMode.POPULATE },
            { element: '-address-locality', field: 'City', mode: pca.fieldMode.POPULATE },
            { element: '-address-administrative-area', field: 'Province', mode: pca.fieldMode.POPULATE },
            { element: '-address-postal-code', field: 'PostalCode' },
          ];
          for (var i = 1; i < loqateFields.length; i++) {
            var field = $('[data-drupal-selector=' + dataSelector + loqateFields[i].element + ']', this);
            loqateFields[i].element = field.attr('id');
          }

          var control = new pca.Address(loqateFields, loqateOptions);
        }
      });
    }
  };
})(jQuery, Drupal, drupalSettings, pca);
