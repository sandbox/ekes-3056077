<?php

namespace Drupal\address_loqate\Plugin\Field\FieldWidget;

use CommerceGuys\Addressing\Country\CountryRepositoryInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Drupal\address\Plugin\Field\FieldWidget\AddressDefaultWidget;

/**
 * Plugin implementation of the 'address_loqate' widget.
 *
 * @FieldWidget(
 *   id = "address_loqate",
 *   label = @Translation("Address (with loqate)"),
 *   field_types = {
 *     "address"
 *   },
 * )
 */
class AddressLoqateWidget extends AddressDefaultWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['#attached']['library'][] = 'address_loqate/address_loqate';
    $config = $this->configFactory->get('address_loqate.loqate_api_config');
    $options = [
      'key' => $config->get('loqate_api_key'),
    ];
    $element['#attached']['drupalSettings']['addressLoqate'] = $options;
    $element['#attributes']['class'][] = 'address-loqate';
    return $element;
  }

}
